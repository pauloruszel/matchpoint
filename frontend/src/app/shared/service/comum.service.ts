import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Aluno} from '../domain/aluno';

@Injectable({
  providedIn: 'root'
})
export class ComumService {

  url: any = 'http://localhost:8080/api';

  constructor(private http: HttpClient) { }

  public getAlunos(): Observable<Aluno[]> {
    return this.http.get<Aluno[]>(`${(this.url)}/aluno`);
  }
}

