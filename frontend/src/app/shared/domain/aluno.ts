import {Professor} from './professor';
import {Endereco} from './endereco';
import {Mensalidade} from './mensalidade';
import {GradeHoraria} from './grade-horaria';

export class Aluno {
  id: number;
  nomeContratante: string;
  cpf: string;
  dataNascimento: Date;
  nomeResponsavel: string;
  parentesco: string;
  cpfParentesco: string;
  telefoneParentesco: string;
  professor: Professor;
  endereco: Endereco;
  mensalidade: Mensalidade;
  gradehoraria: GradeHoraria;
}
