import {Mensalidade} from './mensalidade';

export class Plano {
  id: number;
  tipoPlano: string;
  mensalidade: Mensalidade;
}
