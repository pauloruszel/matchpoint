import {SituacaoAluno} from './situacao-aluno';

export class GradeHoraria {
  id: number;
  data: Date;
  horario: string;
  periodo: string;
  situacaoAluno: SituacaoAluno;

}
