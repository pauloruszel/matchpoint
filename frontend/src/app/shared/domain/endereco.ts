import {Estado} from './estado';

export class Endereco {
  id: number;
  logradouro: string;
  numero: string;
  complemento: string;
  cep: string;
  estado: Estado;

}
