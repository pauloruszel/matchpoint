export class SituacaoAluno {
  id: number;
  tipoSituacaoAluno: string;
  dataSituacaoAluno: Date;
}
