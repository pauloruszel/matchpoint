import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GradehorariaComponent } from './gradehoraria.component';



@NgModule({
  declarations: [GradehorariaComponent],
  imports: [
    CommonModule
  ]
})
export class GradehorariaModule { }
