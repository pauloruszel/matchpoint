import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  ButtonModule, CardModule,
  CodeHighlighterModule,
  InputTextModule,
  MenubarModule,
  PanelMenuModule,
  PanelModule,
  SidebarModule,
  TabViewModule
} from 'primeng';
import {FormsModule} from '@angular/forms';
import {MenuComponent} from './menu.component';


@NgModule({
  declarations: [MenuComponent],
  exports: [
    MenuComponent
  ],
  imports: [
    CommonModule,
    InputTextModule,
    MenubarModule,
    FormsModule,
    ButtonModule,
    TabViewModule,
    CodeHighlighterModule,
    SidebarModule,
    PanelMenuModule,
    PanelModule,
    CardModule
  ]
})
export class MenuModule { }
