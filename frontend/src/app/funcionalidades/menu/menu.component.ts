import {Component, OnInit} from '@angular/core';
import {MenuItem} from 'primeng';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor() {
  }

  items: MenuItem[];

  ngOnInit() {
    this.items = [
      {
        label: 'Home',
        icon: 'pi pi-home',
        routerLink: '/home'
      },
      {
        label: 'Aluno',
        icon: 'pi pi-fw pi-users',
        routerLink: '/listar-aluno',
      },
      {
        label: 'Professor',
        icon: 'pi pi-fw pi-briefcase',
        routerLink: '',
      },
      {
        label: 'Grade Horária',
        icon: 'pi pi-fw pi-calendar',
        routerLink: '',
      },
      {
        label: 'Relatórios',
        icon: 'pi pi-fw pi-folder',
        items: [{
          label: 'Aluno',
          icon: 'pi pi-fw pi-file-pdf',
          routerLink: '',
        },
          {
            label: 'Professor',
            icon: 'pi pi-fw pi-file-pdf',
            routerLink: '',
          }]
      }];
  }

}
