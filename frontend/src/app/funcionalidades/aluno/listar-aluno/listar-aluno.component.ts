import {Component, OnInit} from '@angular/core';
import {Aluno} from '../../../shared/domain/aluno';
import {ListarAlunoService} from './listar-aluno.service';

@Component({
  selector: 'app-listar-aluno',
  templateUrl: './listar-aluno.component.html',
  styleUrls: ['./listar-aluno.component.css']
})
export class ListarAlunoComponent implements OnInit {

  alunos: Aluno[];
  aluno: Aluno = new Aluno();

  constructor(private listarAlunoService: ListarAlunoService) {
  }

  ngOnInit(): void {
    this.listarAlunoService.getAlunos().subscribe(response => {
      this.alunos = response;
    });
  }

  // Função de Teste
  onClickSearch(): void {
    if (this.aluno.nomeContratante !== undefined && this.aluno.nomeContratante !== '') {
      alert(this.aluno.nomeContratante);
    }

    if (this.aluno.cpf !== undefined && this.aluno.cpf !== '') {
      this.listarAlunoService.getAlunoPorCpf(this.aluno.cpf).subscribe(
        response => {
          this.alunos = [];
          this.alunos.push(response);
        });
    } else {
      this.listarAlunoService.getAlunos().subscribe(
        response => {
          this.alunos = response;
        });
    }
  }

}
