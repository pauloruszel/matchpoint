import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListarAlunoComponent} from './listar-aluno.component';

const  routes: Routes = [
  {
    path: '',
    component: ListarAlunoComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [ RouterModule ]
})
export class ListarAlunoRoutingModule { }
