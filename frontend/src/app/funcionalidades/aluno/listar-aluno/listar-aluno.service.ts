import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Aluno} from '../../../shared/domain/aluno';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ListarAlunoService {

  url: any = 'http://localhost:8080/api';
  aluno = new Aluno();

  constructor(private http: HttpClient) {
  }

  public getAlunos = (): Observable<Aluno[]> => this.http.get<Aluno[]>(`${(this.url)}/aluno`);

  public getAlunoPorCpf(cpf: string): Observable<Aluno> {
    return this.http.get<Aluno>(`${(this.url)}/alunocpf/${cpf}`)
      .pipe(
        tap(_ => {
          console.log(`Recuperou o Aluno de cpf=${cpf}`);
        }));
  }


}
