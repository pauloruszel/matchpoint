import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListarAlunoRoutingModule} from './listar-aluno.routing.module';
import {
  ButtonModule,
  CodeHighlighterModule,
  FieldsetModule,
  InputMaskModule,
  InputTextModule,
  PanelModule,
  TableModule
} from 'primeng';
import {ListarAlunoComponent} from './listar-aluno.component';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [ListarAlunoComponent],
  exports: [ListarAlunoComponent],
  imports: [
    CommonModule,
    ListarAlunoRoutingModule,
    PanelModule,
    CodeHighlighterModule,
    InputMaskModule,
    InputTextModule,
    ButtonModule,
    FieldsetModule,
    TableModule,
    FormsModule
  ]
})
export class ListarAlunoModule { }
