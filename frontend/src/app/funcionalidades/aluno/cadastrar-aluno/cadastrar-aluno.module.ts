import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CadastrarAlunoRoutingModule} from './cadastrar-aluno.routing.module';
import {CodeHighlighterModule, InputTextModule, PanelModule} from 'primeng';
import {CadastrarAlunoComponent} from './cadastrar-aluno.component';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [CadastrarAlunoComponent],
  exports: [CadastrarAlunoComponent],
  imports: [
    CommonModule,
    CadastrarAlunoRoutingModule,
    PanelModule,
    CodeHighlighterModule,
    FormsModule,
    InputTextModule
  ]
})
export class CadastrarAlunoModule { }
