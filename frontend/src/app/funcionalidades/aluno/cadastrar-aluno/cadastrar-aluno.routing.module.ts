import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CadastrarAlunoComponent} from './cadastrar-aluno.component';

const  routes: Routes = [
  {
    path: '',
    component: CadastrarAlunoComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [ RouterModule ]
})
export class CadastrarAlunoRoutingModule { }
