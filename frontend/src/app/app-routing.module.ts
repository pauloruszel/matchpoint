import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const  routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./funcionalidades/home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'cadastrar-aluno',
    loadChildren: () => import('./funcionalidades/aluno/cadastrar-aluno/cadastrar-aluno.module').then(m => m.CadastrarAlunoModule)
  },
  {
    path: 'listar-aluno',
    loadChildren: () => import('./funcionalidades/aluno/listar-aluno/listar-aluno.module').then(m => m.ListarAlunoModule)
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/home'
  }

];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes, {useHash: true})
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
