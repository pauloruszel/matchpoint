# Match Point

Projeto para gestão dos alunos e professores da escola de tênis - Match Point - Tennis School Brasília

# FrontEnd

*  lado do cliente(Frontend), criado usando Angular versão 9 e node.js versão 12.16.1.

*  PrimeNG versão 9 - é um framework da Primetek para ser usado para criar telas do frontend.

# BackEnd

*  Lado do servidor(Backend), criado usando Spring boot 2.2.4, Spring Data JPA, Spring Web para trabalhar com RESTful e String MVC e posteriormente Spring security ou OAuth2.

*  Usando Lombok para um código mais limpo. 

*  H2 é um banco em memoria usado para teste. 

*  Postgres é o banco de dados usado, há também como utilizar o H2 que é um banco em memória, apenas comentando os dados do postgres e descomentando os dados do H2.

*  Liquibase é um versionador de registros no banco de dados.

# Observações

* Antes de subir a aplicação, crie um banco com o nome de **match** e o schema com o nome de **escola**.  