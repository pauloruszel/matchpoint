package br.com.matchpoint.gestaoaluno.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
public class Estado implements Serializable {

    private static final long serialVersionUID = 7018860993204264649L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id_estado", unique=true, nullable=false)
    private Long id;

    private String nome;
    private String sigla;
}
