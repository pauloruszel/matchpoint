package br.com.matchpoint.gestaoaluno.service;

import br.com.matchpoint.gestaoaluno.model.SituacaoAluno;
import br.com.matchpoint.gestaoaluno.repository.SituacaoAlunoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SituacaoAlunoService {

    private final SituacaoAlunoRepository situacaoAlunoRepository;

    @Autowired
    public SituacaoAlunoService(SituacaoAlunoRepository situacaoAlunoRepository) {
        this.situacaoAlunoRepository = situacaoAlunoRepository;
    }

    public List<SituacaoAluno> listarTodos(){
        return situacaoAlunoRepository.findAll();
    }

    public Optional<SituacaoAluno> buscarPorId(Long id){
        return situacaoAlunoRepository.findById(id);
    }

    public SituacaoAluno salvar(SituacaoAluno situacaoAluno) {
        return situacaoAlunoRepository.save(situacaoAluno);
    }
}
