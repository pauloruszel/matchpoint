package br.com.matchpoint.gestaoaluno.rest;

import br.com.matchpoint.gestaoaluno.dto.ProfessorDTO;
import br.com.matchpoint.gestaoaluno.dto.mappers.ProfessorMapper;
import br.com.matchpoint.gestaoaluno.service.ProfessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api")
public class ProfessorResource {

    private final ProfessorService professorService;
    private final ProfessorMapper professorMapper;

    @Autowired
    public ProfessorResource(ProfessorService professorService, ProfessorMapper professorMapper) {
        this.professorService = professorService;
        this.professorMapper = professorMapper;
    }

    @GetMapping("/professor")
    public ResponseEntity<List<ProfessorDTO>> listarTodosProfessores(){
        return ok(professorMapper.toListDTOs(professorService.listarTodos()));
    }

    @GetMapping("/professor/{id}")
    public ResponseEntity<ProfessorDTO> buscarProfessorPorId(@PathVariable("id") final Long id){
        return professorService.buscarPorId(id)
                .map(professor -> ok(professorMapper.toDto(professor)))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping(path = "/professor")
    public ResponseEntity<ProfessorDTO> salvar(@Valid @RequestBody ProfessorDTO professorDTO){
        professorService.salvar(professorMapper.toModel(professorDTO));

        return ResponseEntity.status(HttpStatus.CREATED).body(professorDTO);
    }

    @PutMapping("/professor/{id}")
    public ResponseEntity<ProfessorDTO> atualizar(@PathVariable(value = "id") final Long id,
                                           @Valid @RequestBody ProfessorDTO professorDTO) {
        return professorService.buscarPorId(id)
                .map(professor -> {
                    professorService.salvar(professorMapper.toModel(professorDTO));
                    return ok().body(professorMapper.toDto(professor));
                }).orElse(ResponseEntity.notFound().build());

    }

    @DeleteMapping(("/professor/{id}"))
    public ResponseEntity<Long> deletar(@PathVariable final Long id) {
        return professorService.buscarPorId(id)
                .map(professor -> {
                    professorService.deletar(id);
                    return ok().body(id);
                }).orElse(ResponseEntity.notFound().build());

    }

}