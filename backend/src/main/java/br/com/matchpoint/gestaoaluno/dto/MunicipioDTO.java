package br.com.matchpoint.gestaoaluno.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor @AllArgsConstructor
public class MunicipioDTO implements Serializable {

    private static final long serialVersionUID = 1681587985730763464L;

    private Long id;
    private String nome;
    private EstadoDTO estado;
}
