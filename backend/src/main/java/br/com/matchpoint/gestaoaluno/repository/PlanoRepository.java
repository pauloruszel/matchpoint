package br.com.matchpoint.gestaoaluno.repository;

import br.com.matchpoint.gestaoaluno.model.Plano;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlanoRepository extends JpaRepository<Plano, Long> {
}
