package br.com.matchpoint.gestaoaluno.dto.mappers;

import br.com.matchpoint.gestaoaluno.dto.MunicipioDTO;
import br.com.matchpoint.gestaoaluno.model.Municipio;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MunicipioMapper {

    Municipio toModel(MunicipioDTO municipioDTO);

    MunicipioDTO toDto(Municipio municipio);

    List<MunicipioDTO> toListDTOs(List<Municipio> municipios);

}
