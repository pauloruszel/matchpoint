package br.com.matchpoint.gestaoaluno.dto.mappers;

import br.com.matchpoint.gestaoaluno.dto.SituacaoAlunoDTO;
import br.com.matchpoint.gestaoaluno.model.SituacaoAluno;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SituacaoAlunoMapper {

    SituacaoAluno toModel(SituacaoAlunoDTO situacaoAlunoDTO);

    SituacaoAlunoDTO toDto(SituacaoAluno situacaoAluno);

    List<SituacaoAlunoDTO> toListDTOs(List<SituacaoAluno> situacaoAlunos);

}
