package br.com.matchpoint.gestaoaluno.rest;

import br.com.matchpoint.gestaoaluno.dto.MunicipioDTO;
import br.com.matchpoint.gestaoaluno.dto.mappers.MunicipioMapper;
import br.com.matchpoint.gestaoaluno.service.MunicipioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api")
public class MunicipioResource {

    private final MunicipioService municipioService;
    private final MunicipioMapper municipioMapper;

    @Autowired
    public MunicipioResource(MunicipioService municipioService, MunicipioMapper municipioMapper) {
        this.municipioService = municipioService;
        this.municipioMapper = municipioMapper;
    }

    @GetMapping("/municipio")
    public ResponseEntity<List<MunicipioDTO>> listarTodosMunicipios(){
        return ok(municipioMapper.toListDTOs(municipioService.listarTodos()));
    }

    @GetMapping("/municipio/{id}")
    public ResponseEntity<MunicipioDTO> buscarMunicipioPorId(@PathVariable("id") final Long id){
        return municipioService.buscarPorId(id)
                .map(municipio -> ok(municipioMapper.toDto(municipio)))
                .orElse(ResponseEntity.notFound().build());
    }

}