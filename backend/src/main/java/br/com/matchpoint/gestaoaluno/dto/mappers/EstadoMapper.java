package br.com.matchpoint.gestaoaluno.dto.mappers;

import br.com.matchpoint.gestaoaluno.dto.EstadoDTO;
import br.com.matchpoint.gestaoaluno.model.Estado;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EstadoMapper {

    Estado toModel(EstadoDTO estadoDTO);

    EstadoDTO toDto(Estado estado);

    List<EstadoDTO> toListDTOs(List<Estado> estados);

}
