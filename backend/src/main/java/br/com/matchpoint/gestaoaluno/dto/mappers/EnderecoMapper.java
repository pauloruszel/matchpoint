package br.com.matchpoint.gestaoaluno.dto.mappers;

import br.com.matchpoint.gestaoaluno.dto.EnderecoDTO;
import br.com.matchpoint.gestaoaluno.model.Endereco;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EnderecoMapper {

    Endereco toModel(EnderecoDTO enderecoDTO);

    EnderecoDTO toDto(Endereco endereco);

    List<EnderecoDTO> toListDTOs(List<Endereco> enderecos);

}
