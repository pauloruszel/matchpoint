package br.com.matchpoint.gestaoaluno.dto.mappers;

import br.com.matchpoint.gestaoaluno.dto.MensalidadeDTO;
import br.com.matchpoint.gestaoaluno.model.Mensalidade;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MensalidadeMapper {

    Mensalidade toModel(MensalidadeDTO mensalidadeDTO);

    MensalidadeDTO toDto(Mensalidade mensalidade);

    List<MensalidadeDTO> toListDTOs(List<Mensalidade> mensalidades);

}
