package br.com.matchpoint.gestaoaluno.rest;

import br.com.matchpoint.gestaoaluno.dto.EstadoDTO;
import br.com.matchpoint.gestaoaluno.dto.mappers.EstadoMapper;
import br.com.matchpoint.gestaoaluno.service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api")
public class EstadoResource {

    private final EstadoService estadoService;
    private final EstadoMapper estadoMapper;

    @Autowired
    public EstadoResource(EstadoService estadoService, EstadoMapper estadoMapper) {
        this.estadoService = estadoService;
        this.estadoMapper = estadoMapper;
    }

    @GetMapping("/estado")
    public ResponseEntity<List<EstadoDTO>> listarTodosEstados(){
        return ok(estadoMapper.toListDTOs(estadoService.listarTodos()));
    }

    @GetMapping("/estado/{id}")
    public ResponseEntity<EstadoDTO> buscarEstadoPorId(@PathVariable("id") final Long id){
        return estadoService.buscarPorId(id)
                .map(estado -> ok(estadoMapper.toDto(estado)))
                .orElse(ResponseEntity.notFound().build());
    }

}