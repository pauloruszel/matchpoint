package br.com.matchpoint.gestaoaluno.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@NoArgsConstructor @AllArgsConstructor
public class SituacaoAlunoDTO implements Serializable {

    private static final long serialVersionUID = -7948617957485657050L;

    private Long id;
    private String tipoSituacaoAluno;
    private LocalDate dataSituacaoAluno;
}
