package br.com.matchpoint.gestaoaluno.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;


/**
 * @author Paulo.Luz
 *
 */

@Entity
@Data
@NoArgsConstructor @AllArgsConstructor
public class Aluno implements Serializable {

	private static final long serialVersionUID = -198282355975046157L;

	@Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id_aluno", unique=true, nullable=false)
    private Long id;

    private String nomeContratante;
    private String cpf;
    private LocalDate dataNascimento;
    private String nomeResponsavel;
    private String parentesco;
    private String cpfParentesco;
    private String telefoneParentesco;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_professor")
    private Professor professor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_endereco")
    private Endereco endereco;

    @OneToOne
    @JoinColumn(name="id_mensalidade")
    private Mensalidade mensalidade;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_gradehoraria")
    private GradeHoraria gradehoraria;


}