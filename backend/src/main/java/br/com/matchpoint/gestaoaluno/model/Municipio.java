package br.com.matchpoint.gestaoaluno.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
public class Municipio implements Serializable {

    private static final long serialVersionUID = 7138415301547066724L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id_municipio", unique=true, nullable=false)
    private Long id;
    private String nome;

    @ManyToOne
    @JoinColumn(name="id_estado")
    private Estado estado;
}
