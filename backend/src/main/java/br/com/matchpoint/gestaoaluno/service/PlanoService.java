package br.com.matchpoint.gestaoaluno.service;

import br.com.matchpoint.gestaoaluno.model.Plano;
import br.com.matchpoint.gestaoaluno.repository.PlanoRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Getter
public class PlanoService {

    private final PlanoRepository planoRepository;

    @Autowired
    public PlanoService(PlanoRepository planoRepository) {
        this.planoRepository = planoRepository;
    }

    public List<Plano> listarTodos() {
        return planoRepository.findAll();
    }

    public Plano salvar(Plano plano) {
        return planoRepository.save(plano);
    }

}
