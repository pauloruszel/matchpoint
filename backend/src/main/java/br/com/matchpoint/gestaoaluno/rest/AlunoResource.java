package br.com.matchpoint.gestaoaluno.rest;

import br.com.matchpoint.gestaoaluno.dto.AlunoDTO;
import br.com.matchpoint.gestaoaluno.dto.mappers.AlunoMapper;
import br.com.matchpoint.gestaoaluno.service.AlunoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.ResponseEntity.accepted;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class AlunoResource {

    private final AlunoService alunoService;
    private final AlunoMapper alunoMapper;

    @Autowired
    public AlunoResource(AlunoService alunoService, AlunoMapper alunoMapper) {
        this.alunoService = alunoService;
        this.alunoMapper = alunoMapper;
    }


    @GetMapping("/aluno")
    public ResponseEntity<List<AlunoDTO>> listarTodosAlunos() {
        return ok(alunoMapper.toListDTOs(alunoService.listarTodos()));
    }

    @GetMapping("/aluno/{id}")
    public ResponseEntity<AlunoDTO> buscarAlunoPorId(@PathVariable("id") final Long id) {
        return alunoService.buscarPorId(id)
                .map(aluno -> ok(alunoMapper.toDto(aluno)))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping(path = "/aluno")
    public ResponseEntity<AlunoDTO> salvar(@Valid @RequestBody AlunoDTO alunoDTO) {
        alunoService.salvar(alunoMapper.toModel(alunoDTO));

        return ResponseEntity.status(HttpStatus.CREATED).body(alunoDTO);
    }

    @PutMapping("/aluno/{id}")
    public ResponseEntity<AlunoDTO> atualizar(@PathVariable(value = "id") final Long id,
                                              @Valid @RequestBody AlunoDTO alunoDTO) {
        return alunoService.buscarPorId(id)
                .map(aluno -> {
                    alunoService.salvar(alunoMapper.toModel(alunoDTO));
                    return ok().body(alunoMapper.toDto(aluno));
                }).orElse(ResponseEntity.notFound().build());

    }

    @DeleteMapping(("/aluno/{id}"))
    public ResponseEntity<Long> deletar(@PathVariable final Long id) {
        alunoService.deletar(id);

        return accepted().build();

    }

    @GetMapping(("/alunocpf/{cpf}"))
    public ResponseEntity<AlunoDTO> buscarPorCpf(@PathVariable final String cpf) {
        return alunoService.buscarPorCPF(cpf)
                .map(aluno -> ok(alunoMapper.toDto(aluno)))
                .orElse(ResponseEntity.notFound().build());
    }

}
