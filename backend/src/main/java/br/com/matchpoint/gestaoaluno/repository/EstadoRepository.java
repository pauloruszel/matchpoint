package br.com.matchpoint.gestaoaluno.repository;

import br.com.matchpoint.gestaoaluno.model.Estado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoRepository extends JpaRepository<Estado, Long> {
}
