package br.com.matchpoint.gestaoaluno.service;

import br.com.matchpoint.gestaoaluno.model.Municipio;
import br.com.matchpoint.gestaoaluno.repository.MunicipioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MunicipioService {

    private final MunicipioRepository municipioRepository;

    @Autowired
    public MunicipioService(MunicipioRepository municipioRepository) {
        this.municipioRepository = municipioRepository;
    }

    public List<Municipio> listarTodos(){
        return municipioRepository.findAll();
    }

    public Optional<Municipio> buscarPorId(Long id){
        return municipioRepository.findById(id);
    }
}
