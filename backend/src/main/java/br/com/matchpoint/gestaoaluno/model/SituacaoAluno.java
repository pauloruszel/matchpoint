package br.com.matchpoint.gestaoaluno.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "situacaoaluno")
@Data
@NoArgsConstructor
public class SituacaoAluno implements Serializable {

    private static final long serialVersionUID = -3413692571084043719L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id_situacaoaluno", unique=true, nullable=false)
    private Long id;

    private String tipoSituacaoAluno;
    private LocalDate dataSituacaoAluno;

}
