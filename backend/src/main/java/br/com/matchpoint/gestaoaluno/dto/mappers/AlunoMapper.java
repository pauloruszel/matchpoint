package br.com.matchpoint.gestaoaluno.dto.mappers;

import br.com.matchpoint.gestaoaluno.dto.AlunoDTO;
import br.com.matchpoint.gestaoaluno.model.Aluno;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AlunoMapper {

    Aluno toModel(AlunoDTO alunoDTO);

    AlunoDTO toDto(Aluno aluno);

    List<AlunoDTO> toListDTOs(List<Aluno> alunos);

}
