package br.com.matchpoint.gestaoaluno.dto.mappers;

import br.com.matchpoint.gestaoaluno.dto.ProfessorDTO;
import br.com.matchpoint.gestaoaluno.model.Professor;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProfessorMapper {

    Professor toModel(ProfessorDTO professorDTO);

    ProfessorDTO toDto(Professor professor);

    List<ProfessorDTO> toListDTOs(List<Professor> professores);

}
