package br.com.matchpoint.gestaoaluno.repository;

import br.com.matchpoint.gestaoaluno.model.GradeHoraria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GradeHorariaRepository extends JpaRepository<GradeHoraria, Long> {
}
