package br.com.matchpoint.gestaoaluno.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor @AllArgsConstructor
public class EnderecoDTO implements Serializable {

    private static final long serialVersionUID = 941479966082613617L;

    private Long id;
    private String logradouro;
    private String numero;
    private String complemento;
    private String cep;
    private EstadoDTO estado;
}
