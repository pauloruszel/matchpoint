package br.com.matchpoint.gestaoaluno.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;


@Data
@NoArgsConstructor @AllArgsConstructor
public class AlunoDTO implements Serializable {

    private static final long serialVersionUID = 1340169638643407743L;

    private Long id;
    private String nomeContratante;
    private String cpf;
    private LocalDate dataNascimento;
    private String nomeResponsavel;
    private String parentesco;
    private String cpfParentesco;
    private String telefoneParentesco;
    private ProfessorDTO professor;
    private EnderecoDTO endereco;
    private MensalidadeDTO mensalidade;
    private GradeHorariaDTO gradehoraria;
}
