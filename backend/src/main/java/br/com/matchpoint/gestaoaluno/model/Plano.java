package br.com.matchpoint.gestaoaluno.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
public class Plano implements Serializable {

    private static final long serialVersionUID = -3943123655716963205L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id_plano", unique=true, nullable=false)
    private Long id;
    private String tipoPlano;

    @OneToOne
    @JoinColumn(name="id_mensalidade")
    private Mensalidade mensalidade;
}
