package br.com.matchpoint.gestaoaluno.service;

import br.com.matchpoint.gestaoaluno.model.GradeHoraria;
import br.com.matchpoint.gestaoaluno.repository.GradeHorariaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GradeHorariaService {

    private final GradeHorariaRepository gradeHorariaRepository;

    @Autowired
    public GradeHorariaService(GradeHorariaRepository gradeHorariaRepository) {
        this.gradeHorariaRepository = gradeHorariaRepository;
    }

    public List<GradeHoraria> listarTodos(){
        return gradeHorariaRepository.findAll();
    }

    public GradeHoraria salvar(GradeHoraria gradeHoraria) {
        return gradeHorariaRepository.save(gradeHoraria);
    }
}
