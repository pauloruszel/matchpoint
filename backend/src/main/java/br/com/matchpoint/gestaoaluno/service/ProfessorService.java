package br.com.matchpoint.gestaoaluno.service;

import br.com.matchpoint.gestaoaluno.model.Professor;
import br.com.matchpoint.gestaoaluno.repository.ProfessorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProfessorService {

    private final ProfessorRepository professorRepository;

    @Autowired
    public ProfessorService(ProfessorRepository professorRepository) {
        this.professorRepository = professorRepository;
    }

    public List<Professor> listarTodos(){
        return professorRepository.findAll();
    }

    public void salvar(Professor professor) {
        professorRepository.save(professor);
    }

    public Optional<Professor> buscarPorId(Long id){
        return professorRepository.findById(id);
    }

    public void deletar(Long id){ professorRepository.deleteById(id);}

}
