package br.com.matchpoint.gestaoaluno.dto.mappers;

import br.com.matchpoint.gestaoaluno.dto.GradeHorariaDTO;
import br.com.matchpoint.gestaoaluno.model.GradeHoraria;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GradeHorariaMapper {

    GradeHoraria toModel(GradeHorariaDTO gradeHorariaDTO);

    GradeHorariaDTO toDto(GradeHoraria gradeHoraria);

    List<GradeHorariaDTO> toListDTOs(List<GradeHoraria> gradeHorarias);

}
