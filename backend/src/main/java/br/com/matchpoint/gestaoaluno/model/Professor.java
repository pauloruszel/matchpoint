package br.com.matchpoint.gestaoaluno.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
public class Professor implements Serializable {

    private static final long serialVersionUID = -5680963013273252867L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id_professor", unique=true, nullable=false)
    private Long id;

    private String nome;
    private String email;
    private String telefone;
}
