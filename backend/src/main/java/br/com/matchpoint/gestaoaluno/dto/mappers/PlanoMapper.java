package br.com.matchpoint.gestaoaluno.dto.mappers;

import br.com.matchpoint.gestaoaluno.dto.PlanoDTO;
import br.com.matchpoint.gestaoaluno.model.Plano;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PlanoMapper {

    Plano toModel(PlanoDTO planoDTO);

    PlanoDTO toDto(Plano plano);

    List<PlanoDTO> toListDTOs(List<Plano> planos);

}
