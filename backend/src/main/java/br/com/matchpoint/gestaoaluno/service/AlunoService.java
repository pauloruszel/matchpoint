package br.com.matchpoint.gestaoaluno.service;

import br.com.matchpoint.gestaoaluno.model.Aluno;
import br.com.matchpoint.gestaoaluno.repository.AlunoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AlunoService {

    private final AlunoRepository alunoRepository;

    @Autowired
    public AlunoService(AlunoRepository alunoRepository) {
        this.alunoRepository = alunoRepository;
    }

    public void salvar(Aluno aluno) {
        alunoRepository.save(aluno);
    }

    public Optional<Aluno> buscarPorId(Long id){
        return alunoRepository.findById(id);
    }

    public List<Aluno> listarTodos(){
        return alunoRepository.findAll();
    }

    public Optional<Aluno> buscarPorCPF(String cpf){
        return alunoRepository.findByCPF(cpf);
    }

    public void deletar(Long id){ alunoRepository.deleteById(id);}



}
