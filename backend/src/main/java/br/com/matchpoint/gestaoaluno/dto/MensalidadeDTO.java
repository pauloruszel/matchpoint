package br.com.matchpoint.gestaoaluno.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor @AllArgsConstructor
public class MensalidadeDTO implements Serializable {

    private static final long serialVersionUID = 5691778882037834932L;

    private Long id;
    private BigDecimal valor;
    private LocalDate dataVencimento;
}
