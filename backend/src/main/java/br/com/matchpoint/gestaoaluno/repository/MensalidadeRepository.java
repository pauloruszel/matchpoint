package br.com.matchpoint.gestaoaluno.repository;

import br.com.matchpoint.gestaoaluno.model.Mensalidade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MensalidadeRepository extends JpaRepository<Mensalidade, Long> {
}
