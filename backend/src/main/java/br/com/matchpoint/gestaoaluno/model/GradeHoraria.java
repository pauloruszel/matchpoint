package br.com.matchpoint.gestaoaluno.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "gradehoraria")
@Data @NoArgsConstructor
public class GradeHoraria implements Serializable {

    private static final long serialVersionUID = 1842285710910738801L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id_gradehoraria", unique=true, nullable=false)
    private Long id;

    private LocalDate data;
    private LocalTime horario;
    private String periodo;

    @OneToOne
    @JoinColumn(name="id_situacaoaluno")
    private SituacaoAluno situacaoAluno;
}
