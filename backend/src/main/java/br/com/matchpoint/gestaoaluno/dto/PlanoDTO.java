package br.com.matchpoint.gestaoaluno.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor @AllArgsConstructor
public class PlanoDTO implements Serializable {

    private static final long serialVersionUID = -4813698547116857160L;

    private Long id;
    private String tipoPlano;
    private MensalidadeDTO mensalidade;
}
