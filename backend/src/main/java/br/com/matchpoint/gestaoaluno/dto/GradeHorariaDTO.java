package br.com.matchpoint.gestaoaluno.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor @AllArgsConstructor
public class GradeHorariaDTO implements Serializable {

    private static final long serialVersionUID = 2652144385535548579L;

    private Long id;
    private LocalDate data;
    private LocalTime horario;
    private String periodo;
    private SituacaoAlunoDTO situacaoAluno;
}
