package br.com.matchpoint.gestaoaluno.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
public class Mensalidade implements Serializable {

    private static final long serialVersionUID = 7204994223633477048L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id_mensalidade", unique=true, nullable=false)
    private Long id;

    private BigDecimal valor;
    private LocalDate dataVencimento;
}
