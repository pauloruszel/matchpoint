package br.com.matchpoint.gestaoaluno.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor @AllArgsConstructor
public class EstadoDTO implements Serializable {

    private static final long serialVersionUID = -586900119153600233L;

    private Long id;
    private String nome;
    private String sigla;
}
