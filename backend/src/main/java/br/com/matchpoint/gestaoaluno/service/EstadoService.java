package br.com.matchpoint.gestaoaluno.service;

import br.com.matchpoint.gestaoaluno.model.Estado;
import br.com.matchpoint.gestaoaluno.repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EstadoService {

    private final EstadoRepository estadoRepository;

    @Autowired
    public EstadoService(EstadoRepository estadoRepository) {
        this.estadoRepository = estadoRepository;
    }

    public List<Estado> listarTodos(){
        return estadoRepository.findAll();
    }

    public Optional<Estado> buscarPorId(Long id){
        return estadoRepository.findById(id);
    }
}
