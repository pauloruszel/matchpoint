package br.com.matchpoint.gestaoaluno.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor @AllArgsConstructor
public class ProfessorDTO implements Serializable {

    private static final long serialVersionUID = -7785668940620835794L;

    private Long id;
    private String nome;
    private String email;
    private String telefone;
}
