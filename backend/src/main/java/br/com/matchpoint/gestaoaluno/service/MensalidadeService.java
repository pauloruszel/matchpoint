package br.com.matchpoint.gestaoaluno.service;

import br.com.matchpoint.gestaoaluno.model.Mensalidade;
import br.com.matchpoint.gestaoaluno.repository.MensalidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MensalidadeService {

    private final MensalidadeRepository mensalidadeRepository;

    @Autowired
    public MensalidadeService(MensalidadeRepository mensalidadeRepository) {
        this.mensalidadeRepository = mensalidadeRepository;
    }

    public List<Mensalidade> listarTodos(){
        return mensalidadeRepository.findAll();
    }

    public Mensalidade salvar(Mensalidade mensalidade) {
        return mensalidadeRepository.save(mensalidade);
    }
}
