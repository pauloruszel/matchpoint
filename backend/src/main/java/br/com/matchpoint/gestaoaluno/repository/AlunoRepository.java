package br.com.matchpoint.gestaoaluno.repository;

import br.com.matchpoint.gestaoaluno.model.Aluno;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AlunoRepository extends JpaRepository<Aluno, Long> {

    @Query("SELECT a FROM Aluno a WHERE a.cpf = :cpf")
    Optional<Aluno> findByCPF(@Param("cpf") String cpf);
}
