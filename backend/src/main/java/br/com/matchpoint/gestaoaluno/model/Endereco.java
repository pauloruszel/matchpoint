package br.com.matchpoint.gestaoaluno.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
public class Endereco implements Serializable {

    private static final long serialVersionUID = 1296472705845164948L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id_endereco", unique=true, nullable=false)
    private Long id;

    private String logradouro;
    private String numero;
    private String complemento;
    private String cep;

    @ManyToOne
    @JoinColumn(name="id_estado")
    private Estado estado;

}
